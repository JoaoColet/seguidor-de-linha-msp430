#include <msp430.h>
int etapaultrasom=0, status=0, direcao=0; //status: 0=desligado, 1=ligado // direcao: 0=para frente, 1=para tras
unsigned int valanterior = 0, valatual=0; // usado para captura do ultra som
int distancia = 0, contagemultrasom = 0; // distancia do ultra som
int sensor1=0, sensor2=0, sensor3=0, sensor4=0; // sensor 1 LD, sensor 2 MEIO, sensor3 LE considerando andando para frente
int last_state=0;
int fwrd=200;
int ADC10_vetor[7];

int erro_bef=0, erro_sum=0, erro_dif, erro, acao_controle;

void ini_uCon(void){
    WDTCTL = WDTPW | WDTHOLD;                                                   //PARANDO O WDT

    DCOCTL = CALDCO_8MHZ;                                                      //MCLK = 8MHZ --> FREQUENCIA PR� CALIBRADA
    BCSCTL1 = CALBC1_8MHZ;                                                     //ACLK = 32768HZ (LFXT1) --> CRISTAL
    BCSCTL2 = DIVS0 + DIVS1;                                                    //SMCLK = 1MHZ --> FATOR DE DIVISAO 8
    BCSCTL3 = XCAP0 + XCAP1;                                                    //C = 12pF --> CAPACITORES PARA O LFXT1

   while(BCSCTL3 & LFXT1OF);                                                   //SAI DO LOOP PARA LFXT1 EST�VEL

    __enable_interrupt();                                                       //HABILITA A GERA��O DE INTERRUP��ES
}

void ini_P1_P2(void){
    /* P1.1 sensor frontal, P1.2 sensor traseiro, entradas para o modo captura
     * P1.3 chave S2
     */
    P1DIR = ~(BIT1 + BIT2 + BIT3);
    P1REN = BIT3;
    P1OUT = BIT3;
    P1IES = BIT3;
    P1IE = BIT3;
    P1SEL |= BIT1+BIT2;
    /* P2.0, P2.5 nivel alto e P2.2, P2.4 nivel baixo = carrinho anda para frente  (ponte h)
     * P2.0, P2.5 nivel baixo e P2.2, P2.4 nivel alto = carrinho anda para tras
     * todas as portas iguais carrinho fica parado
     * */
    P2DIR = 0xFF;
    P2OUT = 0;   // P2.0(AIN1), P2.5(BIN1) e P2.2(AIN2) E P2.3(BIN2),
    P2SEL |= BIT1 + BIT4;  //SAIDA DO PWM P2.1 E P2.4
    P2SEL2 &= ~(BIT1 + BIT4);
}


void ini_Timer0(void){
    //SMCLK 1MHz, inicialmente parado, iniciado apos clicar no botao S2
    TA0CTL = TASSEL1;
    /* modulo 0 usado para o sensor frontal, iniciado ap�s o clique do botao S2
     * modulo 1 usado para o sensor traseiro, iniciado ap�s o clique do botao S2
     * modulo 2 usado para o gatilho de convers�o do ADC10
     */
    TA0CCTL0 = 0;
    TA0CCTL1 = 0;
    TA0CCTL2 = 0;
}

void ini_TimerA1_PWM(void){
    // timer para a ponte H que controla os motores
    // ACLK no modo up com freq de 65Hz
    TA1CTL = TASSEL0 + MC0;
    TA1CCR0 = 500;

    TA1CCTL1 = OUTMOD0 + OUTMOD1 + OUTMOD2 + OUT;  //SAIDA PWM PARA O MOTOR DA ESQUERDA P2.1
    TA1CCR1 = 200;  // controla a velocidade dos 2 motores da esquerda

    TA1CCTL2 = OUTMOD0 + OUTMOD1 + OUTMOD2 + OUT;  // SAIDA PWM PARA MOTOR DA DIREITA P2.4
    TA1CCR2 = 200;  // controla a velocidade dos 2 motores da direita
}

void ini_ADC10(void){
    ADC10CTL0 = ADC10SHT0 + MSC + ADC10ON + ADC10IE;    //8xClock p/ captura do sinal, m�ltiplas convers�es; Ligar e habilitar interrup��o
    ADC10CTL1 = INCH2 + INCH1 + CONSEQ0;                //Come�a pelo canal A6(P1.6), sequ�ncia de canais
    ADC10AE0 = BIT0 + BIT4 + BIT5 + BIT6;               //Seta as portas como anal�gicas
    ADC10CTL0 |= ENC;                                   //Habilita a convers�o
    ADC10DTC1 = 7;                                      //Quantidade de convers�es realizadas (tamanho do vetor)
    ADC10SA = &ADC10_vetor[0];                          //Endere�o para o DTC
}

#pragma vector=ADC10_VECTOR
__interrupt void ADC10_RTI(void){
    ADC10CTL0 &= ~ADC10SC;
    TA0CCTL2 &= ~CCIE;

    sensor1 = ADC10_vetor[0];   // Sensor conectado ao P1.6 LADO Esquerdo
    if(sensor1 <= 600)          //Filtro para  faixa (branco)
        sensor1 = 0;
    else if(sensor1 >= 800)     //Filtro para faixa (preto)
        sensor1 = 1023;

    sensor2 = ADC10_vetor[1];   // Sensor conectado ao P1.5 MEIO Esquerdo
    if(sensor2 <= 600)
            sensor2 = 0;
    else if(sensor2 >= 800)
            sensor2 = 1023;

    sensor3 = ADC10_vetor[2];   // Sensor conectado ao P1.4 MEIO Direito
    if(sensor3 <= 600)
                sensor3 = 0;
    else if(sensor3 >= 800)
                sensor3 = 1023;

    sensor4 = ADC10_vetor[6];   // Sensor conectado ao P1.0 LADO Direito
    if(sensor4 <= 500)
            sensor4 = 0;
    else if(sensor4 >= 800)
            sensor4 = 1023;

    if(sensor1<150 && sensor2>150 && sensor3>150 && sensor4>150){       //Virar para a esquerda
        if(direcao == 1)        //Quando o carrinho est� indo para tr�s
        {
            //Inverte o sentido da roda de dentro e garante que a de fora vai pra frente
            P2OUT &= ~(BIT0 + BIT3);
            P2OUT |= BIT2 + BIT5;
        }
        else if(direcao == 0)   //Quando o carrinho est� indo para frente
        {
            //Inverte o sentido da roda de dentro e garante que a de fora vai pra frente
            P2OUT &= ~(BIT2 + BIT5);
            P2OUT |= BIT0 + BIT3;
        }
        //Aumenta velocidade para fazer a curva
        TA1CCR1=300;
        TA1CCR2=300;
    }
    else if(sensor1>150 && sensor2>150 && sensor3>150 && sensor4<150){  //Virar para a direita
        if(direcao == 1)
        {
            P2OUT &= ~(BIT2 + BIT5);
            P2OUT |= BIT0 + BIT3;
        }
        else if(direcao == 0)
        {
            P2OUT &= ~(BIT0 + BIT3);
            P2OUT |= BIT2 + BIT5;
        }
        TA1CCR1=300;
        TA1CCR2=300;
    }

    else{                                           //Caso a linha n�o esteja apenas nos sensores dos cantos, usamos um PID
        if(direcao == 1)
        {
            P2OUT &= ~(BIT2 + BIT3);                //Corrige o sentido da roda
            P2OUT |= BIT0 + BIT5;
        }
        else if(direcao == 0)
        {
            P2OUT &= ~(BIT0 + BIT5);
            P2OUT |= BIT2 + BIT3;
        }

        sensor1 = -(1024-sensor1)*2;                //Atribu�mos pesos para os sensores
        sensor2 = -(1024-sensor2);
        sensor3 = (1024-sensor3);
        sensor4 = (1024-sensor4)*2;

        erro = sensor1+sensor2+sensor3+sensor4;     //Calculamos o erro para o PID


        fwrd = 200;                                 //Seta a velocidade padr�o para reta

        acao_controle = erro>>2;                    //Controlador proporcional

        if(acao_controle>=0)                        //Corrige o carrinho para permanecer na linha
        {
            TA1CCR1 = fwrd + acao_controle;         //Acelera rodas da esquerda
            TA1CCR2 = fwrd;
        }
        else
        {
            TA1CCR1 = fwrd;
            TA1CCR2 = fwrd - acao_controle;         //Acelera rodas da direita
        }

    }

    //Seta o endere�o para o vetor do DTC e inicia o timer para gatilho de software em 50ms
    ADC10SA = &ADC10_vetor[0];
    TA0CCTL2 = CCIE;
    TA0CCR2 = TA0R+50000;
}


#pragma vector=PORT1_VECTOR
__interrupt void RTI_P1(void){

    P1IE &= ~BIT3;                      // desabilita interrup��o bit 3
    __delay_cycles(10000);              // debouncer via software
    if((~P1IN & BIT3) && status==0){    // verifica se o status � desligado
        status=1;                       // altera o status para ligado
        TA0CTL |= MC1;                  // inicia o timer dos sensores utlrasom
        TA0CCTL2 = CCIE;                // inicia o timer para gatilho do ADC
        TA0CCR2 = 3000;
        if (direcao==0){                // verifica se a ultima dire�ao era para frente
            P2OUT |= BIT2 + BIT3;       // motores saem do modo parado e giram no sentido horario
            TA0CCTL0 = CCIE;            // habilita a interrup��o do timer para o ultrasom frontal
            TA0CCR0 = TA0R+1000;
        }
        else if (direcao==1){           // verifica se a ultima posi��o era para tras
            P2OUT |= BIT0 + BIT5;       // motores saem do modo parado e giram no sentido anti-horario
            TA0CCTL1 = CCIE;            // habilita itnerrup��o para o timer do ultrasom traseiro
            TA0CCR1 = TA0R+1000;
        }
    }
    else if((~P1IN & BIT3) && status==1){   // verifica se o status � ligado
        status=0;                           // altera status para desligado
        etapaultrasom=0;                    // zera etapa para verifica��o do ultrasom
        P1OUT &= ~BIT7;                     // coloca P1.7 em nivel baixo caso estivesse ativo
        TA0CCTL0 = 0;                       // zera os modulos 0 e 1 desabilitando suas fun��es
        TA0CCTL1 = 0;
        TA0CCTL2 = 0;
        TA0CTL &= ~MC1;                     // para o timer 0
        P2OUT &= ~(BIT0 + BIT2 + BIT3 + BIT5); //para os motores
    }
    P1IFG &= ~BIT3; //limpa flag
    P1IE |= BIT3;
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer0_A0_RTI(void){  // RTI sensor ultrasom frontal
    if(direcao==1){ //entra aqui caso nao obteve resposta e reinicia o as etapas
        TA0CCTL0 &= ~CCIE;
        etapaultrasom=0;
        TA0CCTL1 = CCIE;
        TA0CCR1 = TA0R+1000;            //Corrigir erro de perda de borda da captura
    }
    else{
        if(etapaultrasom==0){           // etapa 0 -> inicio do trigger, coloca P1.7 em nivel alto e aciona o timer para 10us
            P1OUT |= BIT7;
            TA0CCR0 = TA0R + 10;
            etapaultrasom = 1;
        }
        else if(etapaultrasom==1){    // etapa 1 -> coloca novamente em nivel baixo P1.7 e aciona o modo captura esperando interrup�ao por borda de subida e descida
            P1OUT &= ~BIT7;
            TA0CCTL0 |= CM0 + CM1 + CAP + CCIE;
            etapaultrasom=2;
        }
        else if(etapaultrasom==2){ // etapa 2 -> primeira borda(subida) salva o valor de contagens
            valanterior = TA0R;
            etapaultrasom=3;
            TA0CCTL1 = CCIE;            // timer para reiniciar o processo caso nao receba a borda de descida
            TA0CCR1 = TA0R+40000;
        }
        else if(etapaultrasom==3){  // etapa 3 -> segunda borda(descida) salva o valor de contagens
            valatual = TA0R;
            etapaultrasom=0;
            TA0CCTL1 &= ~CCIE;     // cancela o timer de reiniciar
            contagemultrasom = valatual - valanterior; //pega o valor total de cotnagens do timer
            distancia = contagemultrasom/58; // calcula distancia do utlrasom em centimetros, as contagens j� estao na unidade de 1us
            if(distancia < 5 && distancia >0){  // se for menor que 5 centimetros desabilita o modulo 0, aciona o modulo 1 e muda dire��o para sentido anti-horario
                TA0CCTL0 &= ~(CM0 + CM1 + CAP + CCIE);
                TA0CCTL1 |= CCIE;
                TA0CCR1 = TA0R+50000;
                direcao=1;
                P2OUT &= ~(BIT2 + BIT3);
                P2OUT |= BIT0 + BIT5;
            }
            else{  // apenas reinicia as etapas
                TA0CCTL0 &= ~(CM0 + CM1 + CAP);
                TA0CCR0 = TA0R+50000;
            }
        }
    }
}


#pragma vector=TIMER0_A1_VECTOR
__interrupt void Timer0_A1_RTI(void){ // RTI timer0 modulo 1, sensor traseiro
    switch(TA0IV){
    case 2:
        if(direcao==0){
            TA0CCTL1 &= ~CCIE;
            etapaultrasom=0;
            TA0CCTL0 = CCIE;
            TA0CCR0 = TA0R+1000;
        }
        else{
            if(etapaultrasom==0){
                P1OUT |= BIT7;
                TA0CCR1 = TA0R + 10;
                etapaultrasom = 1;
            }
            else if(etapaultrasom==1){
                TA0CCTL1 |= CM0 + CM1 + CAP + CCIE;
                P1OUT &= ~BIT7;
                etapaultrasom=2;
            }
            else if(etapaultrasom==2){
                valanterior = TA0R;
                etapaultrasom=3;
                TA0CCTL0 = CCIE;
                TA0CCR0 = TA0R+40000;
            }
            else if(etapaultrasom==3){
                valatual = TA0R;
                etapaultrasom=0;
                TA0CCTL0 &= ~(CCIE);
                contagemultrasom = valatual - valanterior;
                distancia = contagemultrasom/58;
                if(distancia < 5 && distancia > 0){
                    TA0CCTL1 &= ~(CM0 + CM1 + CAP + CCIE);
                    TA0CCTL0 |= CCIE;
                    TA0CCR0 = TA0R+50000;
                    direcao=0;
                    P2OUT &= ~(BIT0 + BIT5);
                    P2OUT |= BIT2 + BIT3;
                }
                else{
                    TA0CCTL1 &= ~(CM0 + CM1 + CAP);
                    TA0CCR1 = TA0R+50000;
                }
            }
        }
    break;
    case 4:
        ADC10CTL0 |= ADC10SC;               //Gatilho por software do ADC10
    break;
    }
}

void main(void)
{
    ini_uCon();
    ini_P1_P2();
    ini_Timer0();
    ini_TimerA1_PWM();
    ini_ADC10();
    do{
    }while(1);
}