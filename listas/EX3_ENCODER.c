#include <msp430.h> 

void config_ini(void);
void ini_p1_p2(void);
void ini_TA0(void);
void ini_TA1(void);
int freq = 50;
int duty = 50;


int main(void) {

    WDTCTL = WDTPW | WDTHOLD;   // Stop watchdog timer
    config_ini();
    ini_p1_p2();
    ini_TA0();
    ini_TA1();
    do{

    }
    while(1);


}
void config_ini(void){

    /*
     * MCLK = 16MHz
     * SMCLK = 16HHz/2= 8MHz
     * ACLK = Cristal 32.768
     */

    DCOCTL = CALDCO_16MHZ;
    BCSCTL1 = CALBC1_16MHZ;
    BCSCTL2 = DIVS0 + DIVS1;//Divide por 8 SMCLK = 2MHz
    BCSCTL3 = XCAP0 + XCAP1;
    __enable_interrupt();
    while(BCSCTL3 & LFXT1OF); // Sai deste loop quando clock de LFXT1 estiver estavel

}
void ini_p1_p2(void){
    /*
     * P1.0 = CH1 encoder
     * P1.1 = CH2 encoder
     * P1.2 = Alinhado/ Centralizado
     * P1.3 = Frequencia/Duty
     * P1.6 = Sa�da TA0.1 PWM
     */
    P1DIR = ~(BIT0 + BIT1 );                // BIT 0 e 1 -> encoder BIT 3 -> freq/duty BIT 2 -> alinhado, centralizado
    P1REN= BIT0 + BIT1 ;                    //Resistores
    P1OUT= BIT0 + BIT1 ;                    //Pull up em todos
    P1SEL |= BIT6;                          //Servo motor
    P1SEL2 &= ~(BIT6);
    P1IES= BIT0 + BIT1 ;                    //Habilita a borda de descida
    P1IFG= 0x00;
    P1IE= BIT0 + BIT1 ;                     //Habilita as interrup��es

    P2DIR = 0xFF;                           //Todos sa�da
    P2OUT = 0x00;                           //Todas em nivel baixo
}
void ini_TA0(void){
    /*
     * TIMERA_0 -> PWM
     * TIMERA_1 -> Debouncing das chaves
     */

    TA0CTL = TASSEL1 + ID0 + MC0;//SMCLK, Vai at� TA0CCTR0 e recome�a
    TA0CCTL1 = OUTMOD0 + OUTMOD1 + OUTMOD2 + OUT;
    TA0CCR0 = (1000000/freq)-1; // 1E6/freq_selecionada -1
    TA0CCR1 = ((TA0CCR0+1)/1000) * duty;
}
void ini_TA1(void){
    //frequencia do ACLK = 32768Hz
    TA1CTL = TASSEL0 + MC0; //ACLK +contagem at� TA1CCR0
    TA1CCTL0 = CCIE;  //Interrup�ao de captura
    TA1CCR0 = 0; //para o contador


}
//RTI Da Porta
#pragma vector=PORT1_VECTOR
__interrupt void P1_RTI(void){

    P1IFG &= ~(BIT0 + BIT1); // Limpa as interrup��es
    P1IE &= ~(BIT0 + BIT1); // Desativa as interrup��es

    TA1CCR0= 15 ; //TA1CCR0 = (32768*500E-6)-1 = 15,38
}

#pragma vector=TIMER1_A0_VECTOR
__interrupt void TA1__CCR0_RTI(void){

    TA1CCR0 = 0; //para novamente o contador

    if(~P1IN & BIT0){//valida encoder

            if(~P1IN & BIT1){ //decrementa
                    if (duty>50){
                        duty -= 5;
                        TA0CCR1 = ((((long)TA0CCR0+1)*(duty))/1000) -1;

                    }
            }
            else { //incrementa
                    if(duty<100){
                        duty +=5;
                        TA0CCR1 = (((long)(TA0CCR0+1)*(duty))/1000) -1;

                    }

                }
            }


    else{

    }



        P1IFG &= ~(BIT0); //limpa a flag
        P1IE |= (BIT0); //habilita interrup��o

}
