#include <msp430.h> 

// Definindo constantes para os segmentos
#define     seg_a       BIT0
#define     seg_b       BIT1
#define     seg_c       BIT2
#define     LIGA        BIT3
#define     seg_d       BIT4
#define     seg_e       BIT5
#define     seg_f       BIT6
#define     seg_g       BIT7

// definindo os leds que queremos ligar, (display catodo comum)
#define     ZERO    (seg_a + seg_b + seg_c + seg_d + seg_e + seg_f)
#define     UM      (seg_b + seg_c)
#define     DOIS    (seg_a + seg_b + seg_g + seg_d + seg_e)
#define     TRES    (seg_a + seg_b + seg_c + seg_d + seg_g)
#define     QUATRO  (seg_b + seg_c + seg_f + seg_g)
#define     CINCO   (seg_a + seg_f + seg_g + seg_c + seg_d)
#define     SEIS    (seg_a + seg_g + seg_c + seg_d + seg_e + seg_f)
#define     SETE    (seg_a | seg_b | seg_c)
#define     OITO    (seg_a + seg_b + seg_c + seg_d + seg_e + seg_f + seg_g)
#define     NOVE    (seg_a + seg_b + seg_c + seg_d + seg_f + seg_g)
#define     LETRAA  (seg_a + seg_b + seg_c + seg_e + seg_f + seg_g)
#define     LETRAB  (seg_c + seg_d + seg_e + seg_f + seg_g)
#define     LETRAC  (seg_a + seg_d + seg_e + seg_f)
#define     LETRAD  (seg_b + seg_c + seg_d + seg_e + seg_g)

void config_ini(void);
void ini_P1_P2(void);
void ini_TA0(void);
void ini_TA1(void);
void mostrartecla(void);

int tecla = 0;  // nuemro da tecla clicada
int status = 0; //se esta ligado ou desligado

void main(void)
{
    config_ini();
    ini_P1_P2();
    ini_TA0();
    ini_TA1();

    do{
    }while(1);
}

void ini_P1_P2(void){
    P1DIR = 0xFF;       // Todos como saida
    P1OUT = 0x00;       // por padrao saida em nivel baixo, exceto BIT3 que eh funcao liga e o display eh catodo comum


    P2DIR = BIT0 + BIT1 + BIT2 + BIT3;      // Definindo os bits como saida e os ausentes como entrada
    P2REN = BIT4 + BIT5 + BIT6 + BIT7;      // Habilitando o resistor na entrada
    P2OUT = BIT4 + BIT5 + BIT6 + BIT7;      // Habilitando os resistores como pull-up
    P2IES = BIT4 + BIT5 + BIT6 + BIT7;      // Habilitando a interrupcao por borda de descida,ja que eh res. pull-up
    P2SEL = 0;  // Mudanca para usar os pinos 18~19 de XIN, XOUT para P2.6 e P2.7
    P2IFG=0;
    P2IE = BIT4 + BIT5+ BIT6 + BIT7; //habilita itnerrupções
}

void ini_TA0(void){  // timer0 ira fazer o valor na tela piscar qdo # for pressionado

    TA0CTL = TASSEL1 + ID1; // Definindo a fonte do clock como MCLK + divisão por 4, resultado em 500 kHz
    TA0CCTL0 = CCIE; // Interrupcao habilitada
    TA0CCR0 = 50000; // Contar durante 0,25s

}

void ini_TA1(void){  // timer1 ira ser responsavel pelo debounce

    TA1CTL = TASSEL1;   // Definindo a fonte do clock como SMCLK
    TA1CCTL0 = CCIE;    // Habilitada interrupcao
    TA1CCR0 = 9999;   // tempo de 5ms com clock de 2Mhz
}

// RTI do timer0 do modulo 0 para fazer o display piscar
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer0_RTI(void){
    P1OUT ^= BIT3; // alterna entre ligado e desligado
}

//RTI DA PORTA 2 para o debounce das chaves portas P2.4 a P2.7
#pragma vector=PORT2_VECTOR
__interrupt void RTI_P2(void){
    //  PASSO 1: Desabilitando as interrupcoes das entradas
    P2IE &= ~(BIT4 + BIT5 + BIT6+ BIT7);
    TA1CTL |= MC0;  // Iniciando o timer1

}

// RTI do timer1 do modulo 0
#pragma vector=TIMER1_A0_VECTOR
__interrupt void Timer1_RTI(void){
    // Depois de 5ms entra aqui
    TA1CTL &= ~MC0;  // Para o timer
    /*Para realizar a varredura 1 -> identificamos qual flag foi ativa para saber a linha (P2.4 a P2.7) que são as entradas nivel alto,
     * onde a linha que tiver a tecla pressionada estara em nivel 0
     * 2 -> apos descobir a linha fazemos a varredura da coluna (P2.0 a P2.3), alterando o valor da saida para nivel alto de uma
     * coluna por vez deixando as demais em baixo, quando encontramos a coluna certa a entrada voltara para nivel alto, entrando assim
     * no if e definindo o valor da tecla
     * 3-> após o fim da varredura as colunas devem estar todas em zero e a flag da porta de entrada deve ser limpa
     * */
    switch(P2IFG & (BIT4 + BIT5 + BIT6+ BIT7)){     // Caso alguma das flags esteja setada, descobrimos qual tecla foi pressionada

        case BIT4:
            P2OUT |= BIT0;  //inicia a varredura
            if(P2IN & BIT4){
                tecla = 1;
            }
            P2OUT &= ~BIT0;
            P2OUT |= BIT1;
            if(P2IN & BIT4){
                tecla = 2;
            }
            P2OUT &= ~BIT1;
            P2OUT |= BIT2;
            if(P2IN & BIT4){
                tecla = 3;
            }
            P2OUT &= ~BIT2;
            P2OUT |= BIT3;
            if(P2IN & BIT4){
                tecla = 10; //10 representa o A
            }
            P2OUT &= ~BIT3;
            P2IFG &= ~BIT4;     // Limpa a flag de interrupcao

        break;

        case BIT5:

            P2OUT |= BIT0;
            if(P2IN & BIT5){
                tecla = 4;
            }
            P2OUT &= ~BIT0;
            P2OUT |= BIT1;
            if(P2IN & BIT5){
                tecla = 5;
            }
            P2OUT &= ~BIT1;
            P2OUT |= BIT2;
            if(P2IN & BIT5){
                tecla = 6;
            }
            P2OUT &= ~BIT2;
            P2OUT |= BIT3;
            if(P2IN & BIT5){
                tecla = 11; //11 representa o B
            }
            P2OUT &= ~BIT3;
            P2IFG &= ~BIT5;     // Limpa a flag de interrupcao

        break;

        case BIT6:
            P2OUT |= BIT0;
            if(P2IN & BIT6){
                tecla = 7;
            }
            P2OUT &= ~BIT0;
            P2OUT |= BIT1;
            if(P2IN & BIT6){
                tecla = 8;
            }
            P2OUT &= ~BIT1;
            P2OUT |= BIT2;
            if(P2IN & BIT6){
                tecla = 9;
            }
            P2OUT &= ~BIT2;
            P2OUT |= BIT3;
            if(P2IN & BIT6){
                tecla = 12; // 12 representa o C
            }
            P2OUT &= ~BIT3;
            P2IFG &= ~BIT6;     // Limpa a flag de interrupcao
        break;

        case BIT7:
            P2OUT |= BIT0;
            if(P2IN & BIT7){
                tecla = 15; // representa o *
            }
            P2OUT &= ~BIT0;
            P2OUT |= BIT1;
            if(P2IN & BIT7){
                tecla = 0;
            }
            P2OUT &= ~BIT1;
            P2OUT |= BIT2;
            if(P2IN & BIT7){
                tecla = 14; // represneta o #
            }
            P2OUT &= ~BIT2;
            P2OUT |= BIT3;
            if(P2IN & BIT7){
                tecla = 13; // 13 represente o D
            }
            P2OUT &= ~BIT3;
            P2IFG &= ~BIT7;     // Limpa a flag de interrupcao
        break;
    }

    P2IE |= (BIT4 + BIT5 + BIT6+ BIT7);     // Habilitando o modo de interrupcao novamente
    mostrartecla(); //entra na função para definir a saida P1OUT do display
}

void mostrartecla(void){
    if(tecla == 15 && status == 0) //se o status for 0 e a tecla pressiona for o * o display sera ligado
    {
        status=1; //altera status para ligado
        P1OUT = (BIT3 + seg_d + seg_e + seg_f); //liga o display e mostra a letra L
    }
    else if(tecla==15 && status==1) //se a tecla pressionada for * e o display ele ira desligalo
    {
        status=0; //altera status para desligado
        TA0CTL &= ~MC0; //para o timer para piscar o debouncer caso o mesmo esteja ativo
        P1OUT = 0; // desliga o display
    }
    else if(status == 1 && tecla != 15){ //caso nao for pressionado a tecla * e o display estiver ligado ele apenas mostra o valor da tecla pressioanda
    switch(tecla){
    case 0:
        P1OUT = ZERO;
    break;
    case 1:
        P1OUT = UM;
    break;
    case 2:
        P1OUT = DOIS;
    break;
    case 3:
        P1OUT = TRES;
    break;
    case 4:
        P1OUT = QUATRO;
    break;
    case 5:
        P1OUT = CINCO;
    break;
    case 6:
        P1OUT = SEIS;
    break;
    case 7:
        P1OUT = SETE;
    break;
    case 8:
        P1OUT = OITO;
    break;
    case 9:
        P1OUT = NOVE;
    break;
    case 10:
        P1OUT = LETRAA;
    break;
    case 11:
        P1OUT = LETRAB;
    break;
    case 12:
        P1OUT = LETRAC;
    break;
    case 13:
        P1OUT = LETRAD;
    break;
    case 14:
        TA0CTL ^= MC0;
    break;
    }
    }
}

void config_ini(void){
    WDTCTL = WDTPW | WDTHOLD;   // para contador Watchdog timer

    // Configuracoes do BCS
    // MCLK = DCOCLK ~16MHz
    // SMCLK = DCOCLK / 8 ~ 2 MHz

    DCOCTL = CALDCO_16MHZ;  // Freq calibrada em 16 MHz
    BCSCTL1 = CALBC1_16MHZ;
    BCSCTL2 = DIVS0 + DIVS1;  // DIVS = 8 para SMCLK

    __enable_interrupt();  // seta o bit GIE- permite geracao de interrupcoes
}
