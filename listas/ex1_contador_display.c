#include <msp430.h> 

// renomear cosntantes para os sementos
#define seg_a BIT0
#define seg_b BIT1
#define seg_c BIT2
#define seg_d BIT3
#define seg_e BIT4
#define seg_f BIT5
#define seg_g BIT6
#define seg_p BIT7

// display anodo comum, definimos quais segmentos queremos apagar para cada numero
#define ZERO (seg_g + seg_p)
#define UM (seg_a + seg_d + seg_e + seg_f + seg_g + seg_p)
#define DOIS (seg_c + seg_f + seg_p)
#define TRES (seg_e + seg_f + seg_p)
#define QUATRO (seg_a + seg_d + seg_e + seg_h + seg_p)
#define CINCO (seg_b + seg_e + seg_p)
#define SEIS (seg_b + seg_p)
#define SETE (seg_d + seg_e + seg_f + seg_g + seg_p)
#define OITO (seg_p)
#define NOVE (seg_e + seg_p)
#define PONTO (seg_a + seg_b + seg_c + seg_d + seg_e + seg_f + seg_g)


//inicia globalmente as variaveis que iremos usar
int unidade=0;
int dezena=0;

void ini_ucon(void){

    WDTCTL = WDTPW | WDTHOLD; // WatchDog timer parado

    // Configuracoes do BCS
    // MCLK = DCOCLK ~ 16 MHz
    // SMCLK = DCOCLK / 8 ~ 2 MHz

    DCOCTL = CALDCO_16MHZ;
    BCSCTL1 = CALBC1_16MHZ + XT2OFF;
    BCSCTL2 = DIVS0 + DIVS1;

    __enable_interrupt();
}

void ini_P1_P2(void){
    /*
     *  P1.0 e P1.1 saidas alternado distintas entre nivel alto e baixo
     *  P1.3 entrada com borda de descida
     *  P1.x saida nivel baixo
     *  */

    P1DIR = BIT0 + BIT1 + BIT2+ BIT4 + BIT5 + BIT6 + BIT7; // setando apenas P1.3 como entrada para alterar o contador

    P1REN = BIT3; //habilida o resistor para entrada

    P1OUT = BIT3 + BIT1; // setando pull-down, e saida alternada de p1.0/p1.1

    P1IES = BIT3; // seleciona borda de descida para interrupção

    P1IFG = 0;

    P1IE = BIT3; // habilita a requisição da interupção

    /* config porta 2
     *
     *   P2.X todas saidas, mudar func nativa P2.6 e P2.7
     * */

    P2DIR = 0xFF;

    P2REN = 0;

    P2OUT = 0;

    P2IES = 0;

    P2IFG = 0;

    P2IE = 0;

    P2SEL = 0; // muda os pinos XIN e XOUT para P2.6 e P2.7
}

void ini_Timer0(void){
    // timer para alterar os displays
    TA0CTL = TASSEL1 + MC0; //ja iniciado no modo UP para sempre que chegar no valor desejado ele altrar o display
    TA0CCTL0 = CCIE;
    TA0CCR0 = 9999; // altera de display a cada 5ms
}

void ini_Timer1(void){
    //TIMER PARA AS CHAVES
    TA1CTL = TASSEL1; // inicialmente parado, usado para o debouncer da chave
    TA1CCTL0 = CCIE;
    TA1CCR0 = 9999;
}

void main(void)
{

    ini_ucon();
    ini_P1_P2();
    ini_Timer0();
    ini_Timer1();
	do{

	}while(1);
}

//RTI DA PORTA 1 usado para chave P1.3
#pragma vector=PORT1_VECTOR
__interrupt void RTI_P1(void){
    P1IE &= ~(BIT3); //limpa a requisição do bit 3
    TA1CTL |= MC0; //inicia timer 1 para debouncer
}

// RTI DO timer 1 modulo 0, debouncer para chave P1.3
#pragma vector=TIMER1_A0_VECTOR
__interrupt void RTI_TIMER1(void){
    // entra aqui apos 5ms da interrupcao da porta 1

    TA1CTL &= ~MC0;  //para timer1 e em seguida soma 1 no contador
    unidade++;
    if(unidade>9)
    {
        unidade=0;
        dezena++;
            if(dezena>9)
            {
                dezena=0;
            }
    }
    P1IFG &= ~BIT3; //limpa a flag de interrupção
    P1IE |= BIT3; //reabilita a requisição da porta 3
}

// RTI DO MODULO 0 DO TIMER 0 usado para alternar entre os displays
#pragma vector=TIMER0_A0_VECTOR
__interrupt void RTI_TIMER0(void){
P1OUT ^= (BIT1+BIT0); // alterna o display;
    // os 2 ifs são usados para ver que valor devemos colocar no display selecionado, se o BIT0 estiver ativo será a unidade, se o BIT1 estiver ativo sera a dezena
    if(P1IN & BIT0) // verifica se devemos mostrar o valor da unidade
    {
        switch(unidade){
        case 0:
            P2OUT = ZERO;
        break;
        case 1:
            P2OUT = UM;
        break;
        case 2:
            P2OUT = DOIS;
        break;
        case 3:
            P2OUT = TRES;
        break;
        case 4:
            P2OUT = QUATRO;
        break;
        case 5:
            P2OUT = CINCO;
        break;
        case 6:
            P2OUT = SEIS;
        break;
        case 7:
            P2OUT = SETE;
        break;
        case 8:
            P2OUT = OITO;
        break;
        case 9:
            P2OUT = NOVE;
        break;
        }
    }
    if(P1IN & BIT1) // verifica se devemos mostrar o valor da dezena
    {
        switch(dezena){
        case 0:
            P2OUT = ZERO;
        break;
        case 1:
            P2OUT = UM;
        break;
        case 2:
            P2OUT = DOIS;
        break;
        case 3:
            P2OUT = TRES;
        break;
        case 4:
            P2OUT = QUATRO;
        break;
        case 5:
            P2OUT = CINCO;
        break;
        case 6:
            P2OUT = SEIS;
        break;
        case 7:
            P2OUT = SETE;
        break;
        case 8:
            P2OUT = OITO;
        break;
        case 9:
            P2OUT = NOVE;
        break;
        }
    }
}
