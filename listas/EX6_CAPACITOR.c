#include <msp430.h> 

void ini_ucon(void);
void ini_P1_P2(void);
void ini_TA0(void);
void ini_TA1(void);

double tempo = 0, capacitancia = 0;

void main(void) {
    ini_ucon();
    ini_P1_P2();
    ini_TA0();
    ini_TA1();

    do{
      // Loop
    }while(1);
}

#pragma  vector=TIMER1_A0_VECTOR
__interrupt void TimerA1_CC0_RTI(void){
    tempo = TA1CCR0*0.000000125;

    capacitancia = tempo*0.00000721;

}

void ini_ucon(void){
    WDTCTL = WDTPW | WDTHOLD;   // Para o contador do watchdog timer

    DCOCTL = CALDCO_16MHZ;    // Freq. Calibrada de 16 MHz
    BCSCTL1 = CALBC1_16MHZ;
    BCSCTL2 = DIVA0;
    BCSCTL3 = XCAP0 + XCAP1;  // Capacitor do cristal ~12.5 pF

   // while(BCSCTL3 & LFXT1OF);

    __enable_interrupt();
}

void ini_P1_P2(void){

    P1DIR = BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + BIT6 + BIT7 ;
    P1OUT = 0;
    P1IFG = 0;
    P1SEL |= BIT2;

    P2DIR = 0xFE;
   // P2REN = BIT0;
    P2OUT = 0x00;
    P2IFG = 0;
    P2SEL |= BIT0;
}

void ini_TA0 (void){

    TA0CTL = TASSEL1 + MC0;
    TA0CCTL1 = OUTMOD0 + OUTMOD1 + OUTMOD2 + OUT;
    TA0CCR0 = 65535; //periodo de 7m
    TA0CCR1 = 32768;
}

void ini_TA1 (void){

    TA1CTL = TASSEL1 + MC1;
    TA1CCTL0 = CM0 + CAP + CCIE;

}