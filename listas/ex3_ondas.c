#include <msp430.h> 

void ini_ucon(void){

    WDTCTL = WDTPW | WDTHOLD; // WatchDog timer parado

    // Configuracoes do BCS
    // MCLK = DCOCLK ~ 16 MHz
    // ACLK = LFXT1CLK = 32768 Hz
    // SMCLK = DCOCLK / 8 ~ 2 MHz

    DCOCTL = CALDCO_16MHZ;
    BCSCTL1 = CALBC1_16MHZ;
    BCSCTL2 = DIVS0 + DIVS1;
    BCSCTL3 = XCAP0 + XCAP1;

    while(BCSCTL3 & LFXT1OF);

    __enable_interrupt();
}

void ini_P1_P2(void){
    /*
     * P1 TODOS DESLIGADOS, saidas nivel baixo */

    P1DIR = 0xFF;

    P1REN = 0;

    P1OUT = 0;

    P1IES = 0;

    P1IFG = 0;

    P1IE = 0;

    /* config porta 2
     *
     *   P2.0, P2.1,P2.2 saidas nivel baixo usadas para onda quadrada, demais permanecem como saida nivel baixo
     * */

    P2DIR = 0xFF;

    P2REN = 0;

    P2OUT = 0;

    P2IES = 0;

    P2IFG = 0;

    P2IE = 0;
}

void ini_Timer1(void){
    //TIMER PARA AS ondas
    TA1CTL = TASSEL0 + MC1; //inicia no modo continuo para moder usar os modulos 1 e 2.
    TA1CCTL0 = CCIE;
    TA1CCTL1 = CCIE;    //habilita as interrupões para os 3 modulos
    TA1CCTL2 = CCIE;
    TA1CCR0 = 655;
    TA1CCR1 = 327;   // define o tempo para ondas de acordo com sua frequencia pela formula (ACLK * 1/2f) - 1 = TA1CCRx
    TA1CCR2 = 218;   // ACLK = 32767  f0 = 25 Hz, f1= 50 Hz, f2 = 75 Hz, (a freq sera multiplicado por 2 para q a cada T/2 o nivel de sinal seja alterado
}


void main(void)
{
    ini_ucon();
    ini_P1_P2();
    ini_Timer1();

	do{

	}while(1);
	
}

#pragma vector=TIMER1_A0_VECTOR
__interrupt void RTI_TIMER1_M0(void){
    TA1CCR0 += 655;  //incrementa no contador quando sera o proximo passo para alterar de estado
    P2OUT ^= BIT0;      // altera o nivel da saida, formando assim a onda quadrada
}

#pragma vector=TIMER1_A1_VECTOR
__interrupt void RTI_TIMER1_M1_M2(void){

    switch(TA1IV){
    case 2:
        TA1CCR1 += 327;
        P2OUT ^= BIT1;
    break;
    case 4:
        TA1CCR2 += 218;
        P2OUT ^= BIT2;
    break;
    }
}

