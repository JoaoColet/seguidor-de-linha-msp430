
#include <msp430.h> 
					//CRIA PROTOTIPO DA f(x)
void ini_uCon (void);
void Decodificador_BCD (int Digito);
					//CRIA VARIAVEL GLOBAL E INICIALIZA A MESMA
int Contagem = 0;

int main(void) {

    					//CHAMA FUNCAO QUE CONFIGURA O MICRO DE MANEIRA ESSENCIAL AO FUNCIONAMENTO CORRETO
    ini_uCon();

    do{

    }while(1);
}

void ini_uCon (void){

   					//PARANDO O WATCHDOGTIMER

    WDTCTL = WDTPW + WDTHOLD;

    					//CONFIGURACAO DAS PORTAS, DEFINE OS BITS DE E/S, SAIDA NIVEL BAIXO, ATIVA INTERRUP, SELECIONA BORDA, ZERA VETOR
    P1DIR = ~(BIT6+BIT7);
    P1OUT = BIT6 + BIT7;
    P1REN = BIT6 + BIT7;
    P1IES = BIT6;
    P1IE = BIT6;
    P1IFG = 0x00;

    					//PORTA 2 TUDO SAIDA NIVEL BAIXO
    P2DIR = 0xFF;
    P2OUT = 0x00;
    P2SEL = 0x00;
    P2SEL2 = 0x00;

    					//CONFIGURACAO DOS CLOCKS
    DCOCTL = CALDCO_1MHZ;
    BCSCTL1 = CALBC1_1MHZ;
    BCSCTL2 = DIVS1+DIVS0;

    					//HABILITA INTERRUPCAO
    __enable_interrupt();

}

    #pragma vector=PORT1_VECTOR
    __interrupt void P1_RTI(void){
        				//LIMPA VETOR DE FLAG
        P1IFG = 0x00;
        				//DESABILITA GERACAO DE INTERRUPCAO
        P1IE &= ~BIT3;

        				//BOTA WDT CONTAR PRA FAZER BURLAR O EFEITO DO BOUNCE
        WDTCTL = WDTPW + WDTTMSEL + WDTCNTCL;
        IE1 |= WDTIE;
        IFG1 |= WDTIFG;
}

    #pragma vector=WDT_VECTOR
    __interrupt void WDT_RTI (void){
        //PARA WDT
        WDTCTL = WDTPW + WDTHOLD;

        				//ANALISA O NIVEL LOGICO DOS CANAIS E ASSIM INCREMENTA OU DECREMENTA, TAMBEM VALIDA SE ESTA NO FUNDO OU INICO DA ESCALA
        if(P1IN & BIT7){
            if(Contagem < 9){
            Contagem++;
            }
        }
        else if(!(P1IN & BIT7)){
            if(Contagem > 0){
            Contagem--;
            }
        }

        Decodificador_BCD(Contagem);

        				//HABILITA INTERRUP, ZERA VETOR FLAG.
        P1IFG &= BIT6;
        P1IE |= BIT6;
    }


void Decodificador_BCD (int Digito){

    switch (Digito){
            case 0:
                P2OUT = BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + BIT5;
                break;

            case 1:
                P2OUT = BIT1 + BIT2;
                break;

            case 2:
                P2OUT = BIT0 + BIT1 + BIT6 + BIT3 + BIT4;
                break;

            case 3:
                P2OUT = BIT0 + BIT1 + BIT2 + BIT3 + BIT6;
                break;

            case 4:
                P2OUT = BIT1 + BIT2 + BIT5 + BIT6;
                break;

            case 5:
                P2OUT = ~(BIT1 + BIT4);
                break;

            case 6:
                P2OUT = ~BIT1;
                break;

            case 7:
                P2OUT = BIT0 + BIT1 + BIT2;
                break;

            case 8:
                P2OUT = 0xFF;
                break;

            case 9:
                P2OUT = ~BIT4;
                break;

            default:
                P2OUT = BIT6;
                break;

    }


}
